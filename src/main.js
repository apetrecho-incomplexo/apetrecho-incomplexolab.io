  import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import MuseUI from 'muse-ui'
import 'muse-ui/dist/muse-ui.css'
import theme from 'muse-ui/lib/theme'
import * as colors from 'muse-ui/lib/theme/colors'

// import theme from 'muse-ui/lib/theme'

theme.add('dayglo', {
  primary: '#607d8b',
  secondary: '#7f8583',
  success: '#2f4f4f',
  warning: '#ffeb3b',
}, 'light');

theme.use('dayglo');

Vue.use(MuseUI);


Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
